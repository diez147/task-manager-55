package ru.tsc.babeshko.tm.exception.field;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class ExistsEmailException extends AbstractException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}