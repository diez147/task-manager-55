package ru.tsc.babeshko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.babeshko.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.babeshko.tm.api.endpoint.IUserEndpoint;
import ru.tsc.babeshko.tm.api.service.IPropertyService;
import ru.tsc.babeshko.tm.dto.request.*;
import ru.tsc.babeshko.tm.dto.response.UserLoginResponse;
import ru.tsc.babeshko.tm.dto.response.UserProfileResponse;
import ru.tsc.babeshko.tm.dto.response.UserRegistryResponse;
import ru.tsc.babeshko.tm.dto.response.UserUpdateResponse;
import ru.tsc.babeshko.tm.marker.ISoapCategory;
import ru.tsc.babeshko.tm.dto.model.UserDTO;
import ru.tsc.babeshko.tm.service.PropertyService;

@Category(ISoapCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(host, port);

    @Nullable
    private String adminToken;

    @Nullable
    private UserDTO initUser;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin"));
        adminToken = loginResponse.getToken();
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(null, null, null)));
        userEndpoint.registryUser(new UserRegistryRequest("login", "password", ""));
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("login", "password"));
        Assert.assertNotNull(loginResponse.getToken());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, "login"));
    }

    @Test
    public void removeUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test", "test", ""));
        initUser = registryResponse.getUser();
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(null, initUser.getLogin())));
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, initUser.getLogin()));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test")));
    }

    @Test
    public void updateUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test", "test", ""));
        initUser = registryResponse.getUser();
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        @Nullable String token = loginResponse.getToken();
        @NotNull final String firstName = "first";
        @NotNull final String lastName = "last";
        @NotNull final String middleName = "middle";
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.updateUser(
                        new UserUpdateRequest(null, firstName, lastName, middleName)));
        @NotNull UserUpdateResponse response =
                userEndpoint.updateUser(new UserUpdateRequest(token, firstName, lastName, middleName));
        @Nullable UserDTO user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, initUser.getLogin()));
    }

    @Test
    public void showProfileUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test", "test", ""));
        initUser = registryResponse.getUser();
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        @Nullable String token = loginResponse.getToken();
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.showProfileUser(new UserProfileRequest(null)));
        @NotNull UserProfileResponse response = userEndpoint.showProfileUser(new UserProfileRequest(token));
        @Nullable UserDTO user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, initUser.getLogin()));
    }

    @Test
    public void changePassword() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test", "test", ""));
        initUser = registryResponse.getUser();
        @NotNull String oldPassword = "test";
        @NotNull String newPassword = "new_password";
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test", oldPassword));
        @Nullable String token = response.getToken();
        Assert.assertNotNull(token);
        userEndpoint.changePassword(new UserChangePasswordRequest(token, newPassword));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", oldPassword)));
        @NotNull final UserLoginResponse responseAfterChange = authEndpoint.login(
                new UserLoginRequest("test", newPassword));
        Assert.assertNotNull(responseAfterChange.getToken());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, initUser.getLogin()));
    }

    @Test
    public void lockUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test", "test", ""));
        initUser = registryResponse.getUser();
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(null, initUser.getLogin())));
        userEndpoint.lockUser(new UserLockRequest(adminToken, initUser.getLogin()));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test")));
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, initUser.getLogin()));
    }

    @Test
    public void unlockUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test", "test", ""));
        initUser = registryResponse.getUser();
        userEndpoint.lockUser(new UserLockRequest(adminToken, initUser.getLogin()));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test")));
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(null, initUser.getLogin())));
        userEndpoint.unlockUser(new UserUnlockRequest(adminToken, initUser.getLogin()));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test", "test"));
        @Nullable String token = response.getToken();
        Assert.assertNotNull(token);
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, initUser.getLogin()));
    }

}