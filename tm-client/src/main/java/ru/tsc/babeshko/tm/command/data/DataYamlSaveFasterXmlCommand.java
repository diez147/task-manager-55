package ru.tsc.babeshko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.DataYamlSaveFasterXmlRequest;
import ru.tsc.babeshko.tm.enumerated.Role;

@Component
public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data to yaml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
