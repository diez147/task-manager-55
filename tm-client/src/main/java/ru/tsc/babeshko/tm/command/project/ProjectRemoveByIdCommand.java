package ru.tsc.babeshko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.babeshko.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.babeshko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;
import ru.tsc.babeshko.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectEndpoint().removeProjectById(new ProjectRemoveByIdRequest(getToken(), id));
    }

}