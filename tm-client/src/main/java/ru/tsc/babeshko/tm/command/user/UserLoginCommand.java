package ru.tsc.babeshko.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.UserLoginRequest;
import ru.tsc.babeshko.tm.dto.response.UserLoginResponse;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.util.TerminalUtil;

@Component
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "login";

    @NotNull
    public static final String DESCRIPTION = "Log in";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        @NotNull String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @Nullable final String token = getAuthEndpoint().login(request).getToken();
        setToken(token);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}