package ru.tsc.babeshko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.UserLockRequest;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.util.TerminalUtil;

@Component
public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    public static final String DESCRIPTION = "Lock user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        @NotNull String login = TerminalUtil.nextLine();
        getUserEndpoint().lockUser(new UserLockRequest(getToken(), login));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}