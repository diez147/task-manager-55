package ru.tsc.babeshko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.UserLogoutRequest;
import ru.tsc.babeshko.tm.enumerated.Role;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "Log out";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}