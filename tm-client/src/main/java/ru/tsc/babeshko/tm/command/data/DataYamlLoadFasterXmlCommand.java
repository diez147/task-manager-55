package ru.tsc.babeshko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.DataYamlLoadFasterXmlRequest;
import ru.tsc.babeshko.tm.enumerated.Role;

@Component
public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from yaml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        getDomainEndpoint().loadDataYamlFasterXml(new DataYamlLoadFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}