package ru.tsc.babeshko.tm.command.data;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.babeshko.tm.command.AbstractCommand;

@Getter
@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

}