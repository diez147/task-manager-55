package ru.tsc.babeshko.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.api.repository.ICommandRepository;
import ru.tsc.babeshko.tm.api.service.ICommandService;
import ru.tsc.babeshko.tm.command.AbstractCommand;

import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        if (argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }

}