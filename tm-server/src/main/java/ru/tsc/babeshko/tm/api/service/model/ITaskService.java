package ru.tsc.babeshko.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByUserId(@Nullable String userId);

}
